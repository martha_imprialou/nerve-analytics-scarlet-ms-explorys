PatientDemographics<-function(naivePatients){
  demographicPatient<-as.data.table(dbGetQuery(env$sm,"select explorys_patient_id, 
                                             std_gender,
                                             birth_year
                                             from supermart_200_S_02_10_2017.v_demographic")) 
  #demographicPatientNaive<-demographicPatient[explorys_patient_id%in%naivePatients$explorys_patient_id]
  #deceasedNaive<-unique(demographicPatientNaive[is_deceased=='t'])
}


# this function was used to check other definition of naive patients

# getNaivePatients<-function(){
#   
#   #first get first MS record from medical history for all patients
#   MSmedicalHistory<-as.data.table(
#   dbGetQuery(env$sm,"SELECT * from supermart_200_S_02_10_2017.v_medical_history where icd_code LIKE '340%' OR icd_code LIKE 'G35%'"))
#   MSmedicalHistory$contact_date<-as.Date(MSmedicalHistory$contact_date)
#   MSmedicalHistory<-MSmedicalHistory[order(explorys_patient_id,contact_date)]
#   firstMSRecordMedicalHistory<-MSmedicalHistory[,.(dateMedicalHistoryMS=contact_date[1]),
#                                                 by=explorys_patient_id]
#   
#   #get first MS diagnosis from diagnsosis table for all patients
#   MSdiagnosis<-as.data.table(
#     dbGetQuery(env$sm,"SELECT * from supermart_200_S_02_10_2017.v_diagnosis where icd_code LIKE '340%' OR icd_code LIKE 'G35%'"))
#   MSdiagnosis$diagnosis_date<-as.Date(MSdiagnosis$diagnosis_date)
#   MSdiagnosis<-MSdiagnosis[order(explorys_patient_id,diagnosis_date)]
#   firstMSRecordDiagnosis<-MSdiagnosis[,.(dateDiagnosisMS=diagnosis_date[1]),
#                                       by=explorys_patient_id]
#   #get first prescription for MS drug for all patients
#   MSdrugsTables<-DrugCounts()
#   firstMSdrug<-MSdrugsTables$firstPrescDate
#   
#   #merge together the three tables and get the min date between the first MS record date,
#   #the first MS diagnosis date and the first MS drug presc date
#   firstMSmedicalHistDiagnosis<-merge(firstMSRecordMedicalHistory,firstMSRecordDiagnosis,by='explorys_patient_id',all.x=T,all.y=T)
#   firstMSmedicalHistDiagnosis<-merge(firstMSmedicalHistDiagnosis,firstMSdrug,by='explorys_patient_id',all.x=T,all.y=T)
#   firstMSmedicalHistDiagnosis<-transform(firstMSmedicalHistDiagnosis,minDate=pmin(dateMedicalHistoryMS,dateDiagnosisMS,firstPrescDate,na.rm = T))
# 
#   #get the list of all encounters for all patients happening after 2010
#   listOfEncounters<-unique(as.data.table(dbGetQuery(env$sm,"SELECT explorys_patient_id,
#                                                     encounter_date from supermart_200_S_02_10_2017.v_encounter")))
#   listOfEncounters$encounter_date<-as.Date(listOfEncounters$encounter_date)
#   firstEncounter2010<-listOfEncounters[encounter_date>='2010-01-01',.(encounter_date=min(encounter_date),
#                                                                       lastEncounter=max(encounter_date)),
#                                        by=explorys_patient_id]
#   #merging medical history and diagnosis dates with firstEncounterAfter2010 table, calculate the diff in days and
#   #get patients present in the system at least 1y before the MS diagnosis
#   patFirstPrescFirstEncDate<- merge(firstMSmedicalHistDiagnosis,firstEncounter2010,by='explorys_patient_id',all.x=T)
#   patFirstPrescFirstEncDate$diff <- with(patFirstPrescFirstEncDate,as.integer(difftime(minDate,encounter_date,units = 'days')))
#   naivePatients <- patFirstPrescFirstEncDate[(diff>=360)]
#   return(naivePatients)
#   
#   
# }