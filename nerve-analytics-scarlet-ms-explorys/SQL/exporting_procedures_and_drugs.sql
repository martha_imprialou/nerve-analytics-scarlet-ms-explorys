select distinct cpt_concept from  "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."v_procedure_cpt" where short_text is null

--Retrieve procedures related to MS
select cpt_concept, short_text from  "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."v_procedure_cpt"
	where cpt_concept in ('J1595', 'Q3027', 'J1826', 'Q3025', 'Q3028', 'Q3026', 'J2323', 'J1830', 'J0202', 'Q9979', 'J9010', 'J9310', 'J7513', 'J7500', 'J7501')
	group by cpt_concept, short_text

select * from "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS".v_drug_rxnconso where explorys_patient_id in (136577)

select * from "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."AllNaivePatients"

--Procedures: directly from SUPERMART
select count(distinct cpt_concept) from "db1"."SUPERMART_200_S_02_10_2017"."v_procedure";

select count(distinct short_text) from "db1".XREF.cpt;



select procedures.explorys_patient_id as explorys_patient_id, upper(regexp_replace(cpt.short_text, '([^a-zA-Z0-9])','-')) as procedure_short_text, count(procedures.encounter_join_id) as nencounters
from "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."AllNaivePatients" as patients
	inner join "db1"."SUPERMART_200_S_02_10_2017"."v_procedure"	as procedures
	on patients.x = procedures.explorys_patient_id
	inner join db1.XREF.cpt as cpt
	on procedures.cpt_concept = cpt.cpt_code
	--inner join "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."v_drug_rxnconso" as drugs on patients.x = drugs.explorys_patient_id
	where cpt.short_text is not Null
	group by explorys_patient_id, procedure_short_text order by nencounters desc
	;

--Procedures: from intermediate table
select distinct short_text from "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."v_procedure_cpt" order by short_text desc;

select procedures.explorys_patient_id as explorys_patient_id, procedures.short_text as procedure_short_text, count(procedures.encounter_join_id) as nencounters
from "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."AllNaivePatients" as patients
	inner join "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."v_procedure_cpt"
	as procedures on patients.x = procedures.explorys_patient_id
	--inner join "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."v_drug_rxnconso" as drugs on patients.x = drugs.explorys_patient_id
	where procedures.short_text is not Null
	group by explorys_patient_id, procedures.short_text order by nencounters desc
	;

select drugs.explorys_patient_id as explorys_patient_id, drugs.rx_cui as rxcui, count(drugs.encounter_join_id) as nencounters  from "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."AllNaivePatients" as patients
	--inner join "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."v_procedure_cpt" as procedures on patients.x = procedures.explorys_patient_id
	inner join "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."v_drug_rxnconso" as drugs on patients.x = drugs.explorys_patient_id
	where drugs.rx_cui is not Null
	group by drugs.explorys_patient_id, rxcui order by nencounters desc
	;

select * from "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS".tmp_master_table where zip_code is not Null;

select count(distinct short_text),count(distinct long_text),count(*) from "db1"."XREF"."cpt";

SELECT PATIENT_ID,ENCOUNTER_TYPE,count(encounter_ID) as nencounters
FROM "SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."tmp_master_table" group by patient_id,encounter_type;

select distinct encounter_type  FROM "SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."tmp_master_table" where encounter_type is not null and length(encounter_type)>0;