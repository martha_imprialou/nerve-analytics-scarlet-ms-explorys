DROP TABLE IF EXISTS "SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."corticosteroid_ndc_codes";
CREATE TABLE SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.corticosteroid_ndc_codes (ndc_code VARCHAR(255) not null);
COPY SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.corticosteroid_ndc_codes FROM LOCAL 'Y:\mckinsey_global\Explorys Upload\ndc_codes_corticosteroids.csv';
select count(*) from SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.corticosteroid_ndc_codes;

select count(distinct ndc_code) from SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.corticosteroid_ndc_codes;
                
DROP TABLE IF EXISTS "SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."corticosteroid_rx_codes";
CREATE TABLE SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.corticosteroid_rx_codes (rxcui VARCHAR(255) not null, name VARCHAR(1024), tty VARCHAR(255));
COPY SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.corticosteroid_rx_codes FROM LOCAL 'Y:\mckinsey_global\Explorys Upload\rxcui_corticosteroids.csv' PARSER fcsvparser();
select count(*) from SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.corticosteroid_rx_codes;

select distinct * from SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.corticosteroid_rx_codes;

select rxcui,count(explorys_patient_id) n, r.name from SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.corticosteroid_rx_codes r
left join "db1"."SUPERMART_200_S_02_10_2017"."v_drug" d on r.rxcui = d.rx_cui group by rxcui, r.name order by n desc;

select * from "db1"."SUPERMART_200_S_02_10_2017"."v_drug"where encounter_join_id is not null  order by encounter_join_id ;

select encounter_date, prescription_date, abs(datediff(hour, encounter_date, prescription_date)) from "db1"."SUPERMART_200_S_02_10_2017"."v_drug" ;

select count(1) as ntotal, count( abs(datediff(hour, encounter_date, prescription_date))<24) as nmatch from "db1"."SUPERMART_200_S_02_10_2017"."v_drug" ;

--look for a unique record
select count(record_id_hash) as c, count(distinct record_id_hash) as cd, count(1) as cALL, count(distinct encounter_record_id_hash), count(encounter_record_id_hash),
count(encounter_join_id) as cej, count(distinct encounter_join_id) as cdej,
from "db1"."SUPERMART_200_S_02_10_2017"."v_drug";

-- OK this means we use record_id_hash as the unique reference into this table
select count(1) as ntotal, count(record_id_hash) as nrecord_id_hash, sum(case when record_id_hash is null then 1 else 0 end) as nnull from db1.SUPERMART_200_S_02_10_2017.v_drug;

select encounter_join_id, NVL(std_dosage,std_drug_desc) from db1.SUPERMART_200_S_02_10_2017.v_drug where encounter_join_id is not null order by encounter_join_id;

select distinct ndc_code,display_ndc from "db1"."SUPERMART_200_S_02_10_2017"."v_drug" where ndc_code <> display_ndc;

select ndc_code, length(btrim(ndc_code)) as l from "db1"."SUPERMART_200_S_02_10_2017"."v_drug" order by l desc;

select length(btrim(ndc_code)) as l, count(1) as c from "db1"."SUPERMART_200_S_02_10_2017"."v_drug" group by l order by c desc;

select ndc_code like '%,%' as contains_comma, length(btrim(ndc_code)) as l, count(1) as c from "db1"."SUPERMART_200_S_02_10_2017"."v_drug" group by l,contains_comma order by c desc;

select all_ndc_codes like '%,%' as contains_comma, length(btrim(all_ndc_codes)) as l, count(1) as c from "db1"."SUPERMART_200_S_02_10_2017"."v_drug" group by l,contains_comma order by c desc;

select display_ndc like '%,%' as contains_comma, length(btrim(display_ndc)) as l, count(1) as c from "db1"."SUPERMART_200_S_02_10_2017"."v_drug" group by l,contains_comma order by c desc;

-- Break ndc_codes into several parts
-- NB: use the v_drug s as generator of row_numbers, and limit to checking for 100 codes... haven't checked this assumption
--TODO: validate we dont get more than 100 codes per record
create or replace view "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."v_drug_code_lookup" as (
select distinct record_id_hash, 'NDC' as code_type, regexp_replace(btrim(display_ndc),'-','') as ndc_code from SUPERMART_200_S_02_10_2017.v_drug where ndc_code is not null
union
select distinct record_id_hash, 'NDC' as code_type, regexp_replace(btrim(split_part(ndc_code, ',', n.num)), '-','')  as ndc_code from SUPERMART_200_S_02_10_2017.v_drug 
		cross join (select row_number() over () as num from SUPERMART_200_S_02_10_2017.v_drug limit 100) as n
		where split_part(ndc_code, ',', n.num) <> '' and ndc_code is not null
union
select distinct record_id_hash, 'NDC' as code_type, regexp_replace(btrim(split_part(all_ndc_codes, ',', n.num)), '-','')  as ndc_code from SUPERMART_200_S_02_10_2017.v_drug 
		cross join (select row_number() over () as num from SUPERMART_200_S_02_10_2017.v_drug limit 100) as n
		where split_part(all_ndc_codes, ',', n.num) <> '' and all_ndc_codes is not null
);

-- use new view
select c.ndc_code, count(distinct d.record_id_hash) as nrecords, count(distinct d.explorys_patient_id) as npatients
from SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.corticosteroid_ndc_codes c
left join SUPERMART_200_S_02_10_2017.v_drug d
	on c.ndc_code = l.ndc_code and l.code_type='NDC'
left join "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS".NewNaivePatients_27022017 p
	on d.explorys_patient_id = p.V1
left join "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."v_drug_code_lookup" l
	on d.record_id_hash = l.record_id_hash
group by c.ndc_code, 
;

select *
from SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.corticosteroid_ndc_codes c
join XREF.ndc_product x on regexp_replace(x.product_ndc,'-','') = substr(c.ndc_code,1,8)
;

select length(ndc_code) as l, count(1) as c from SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.corticosteroid_ndc_codes group by l order by c;

select length(regexp_replace(product_ndc,'-','')) l, count(1) c from XREF.ndc_product group by l order by c;

select * from XREF.ndc_product;

--test NB: takes a while, around 1min
select record_id_hash, 'NDC' as code_type, all_ndc_codes, regexp_replace(btrim(split_part(all_ndc_codes, ',', n.num)), '-','')  as ndc_code from SUPERMART_200_S_02_10_2017.v_drug 
		cross join (select row_number() over () as num from SUPERMART_200_S_02_10_2017.v_drug limit 100) as n
		where split_part(all_ndc_codes, ',', n.num) <> '' and all_ndc_codes is not null

--NB: WARNING THIS WILL TAKE A FEW MINUTES
select count(*) from "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."v_drug_code_lookup";


---------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------
--This convoluted query is necessary to split the whitespace-separated allergy list in order to count
-- specific allergies separately for each patient.
-- Query: count of encounters per allergy and per patient for all naive patients.
select explorys_patient_id,allergy,x.concept_name, count(1) as nencounters from 
	(select explorys_patient_id, std_allergy as allergies, num as allergy_idx, split_part(std_allergy, ' ', n.num) as allergy from SUPERMART_200_S_02_10_2017.v_allergy 
		cross join SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.nums as n
		where split_part(std_allergy, ' ', n.num) <> '' 
	) as t
left join XREF.snomed as x on allergy = x.concept_id
group by explorys_patient_id,allergy,x.concept_name
order by explorys_patient_id,nencounters desc;

-- counts aggregated over patients
select allergy,x.concept_name, count(1) as nencounters from 
	(select explorys_patient_id, std_allergy as allergies, num as allergy_idx, split_part(std_allergy, ' ', n.num) as allergy from SUPERMART_200_S_02_10_2017.v_allergy 
		cross join SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.nums as n
		where split_part(std_allergy, ' ', n.num) <> ''
	) as t
left join XREF.snomed as x on allergy = x.concept_id
group by allergy,x.concept_name
order by nencounters desc;

SELECT explorys_patient_id,std_allergy,encounter FROM "SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."v_patient_allergy_enc";

SELECT count(distinct std_allergy) FROM SUPERMART_200_S_02_10_2017.v_allergy as a
inner join SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.AllNaivePatients as p on a.explorys_patient_id = p.x;

SELECT distinct std_allergy FROM SUPERMART_200_S_02_10_2017.v_allergy as a
inner join SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.AllNaivePatients as p on a.explorys_patient_id = p.x;

create table SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.nums(num INT);

insert into SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.nums values(9);

select explorys_patient_id, allergies,num,allergy,x.concept_name from 
	(select explorys_patient_id, std_allergy as allergies, num, split_part(std_allergy, ' ', n.num) as allergy from SUPERMART_200_S_02_10_2017.v_allergy as a
		cross join SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.nums as n
		inner join SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.AllNaivePatients as p on a.explorys_patient_id = p.x
		where (length(std_allergy)>(9+1)*1) and split_part(std_allergy, ' ', n.num) <> ''
	) as t
left join XREF.snomed as x on allergy = x.concept_id order by explorys_patient_id, num;

select std_allergy, x.concept_name  from SUPERMART_200_S_02_10_2017.v_allergy as a
left join XREF.snomed as x on a.std_allergy = x.concept_id group by std_allergen_type, concept_name;

select 

select * from SUPERMART_200_S_02_10_2017.v_habit

