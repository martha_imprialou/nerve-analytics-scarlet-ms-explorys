select version();

select count(distinct x) as npatients from "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."AllNaivePatients" as patients

select count(distinct x) as npatients_with_procedures from "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."AllNaivePatients" as patients
	inner join "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."v_procedure_cpt" as procedures on patients.x = procedures.explorys_patient_id;
	
select count(distinct x) as npatients_with_both from "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."AllNaivePatients" as patients
	inner join "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."v_procedure_cpt" as procedures on patients.x = procedures.explorys_patient_id
	inner join "db1"."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS"."v_drug_rxnconso" as drugs on patients.x = drugs.explorys_patient_id
	;

