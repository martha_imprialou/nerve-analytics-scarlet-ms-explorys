create table db1."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS".tmp_rmuil_obf as select * from db1.SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.AllNaivePatients;

alter table db1."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS".tmp_rmuil_obf rename column x to explorys_patient_id;

select explorys_patient_id,r, row_number() over() as obfid from (select explorys_patient_id,random() as r from db1."SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS".tmp_rmuil_obf order by r) as a order by r;

select * from table;