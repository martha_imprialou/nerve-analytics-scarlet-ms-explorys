create or replace view db1.SANDBOX_MCKINSEY_MULTIPLE_SCLEROSIS.v_drug_code_lookup as (
  select distinct record_id_hash, 'NDC' as code_type, regexp_replace(btrim(display_ndc),'-','') as ndc_code from SUPERMART_200_S_02_10_2017.v_drug where ndc_code is not null
  union
  select distinct record_id_hash, 'NDC' as code_type, regexp_replace(btrim(split_part(ndc_code, ',', n.num)), '-','')  as ndc_code from SUPERMART_200_S_02_10_2017.v_drug 
  cross join (select row_number() over () as num from SUPERMART_200_S_02_10_2017.v_drug limit 100) as n
  where split_part(ndc_code, ',', n.num) <> '' and ndc_code is not null
  union
  select distinct record_id_hash, 'NDC' as code_type, regexp_replace(btrim(split_part(all_ndc_codes, ',', n.num)), '-','')  as ndc_code from SUPERMART_200_S_02_10_2017.v_drug 
  cross join (select row_number() over () as num from SUPERMART_200_S_02_10_2017.v_drug limit 100) as n
  where split_part(all_ndc_codes, ',', n.num) <> '' and all_ndc_codes is not null
);


